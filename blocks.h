//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/  /*Command*/                           /*Update Interval*/  /*Update Signal*/
	{"",      "~/.local/bin/statusbar/microphone",  0,                   1},
	{"VOL ",  "~/.local/bin/statusbar/volume",      0,                   2},
	{"LUX ",  "~/.local/bin/statusbar/backlight",   0,                   3},
	{"BAT ",  "~/.local/bin/statusbar/battery",     10,                  0},
	{"CPU ",  "~/.local/bin/statusbar/cpu",         30,                  0},
	{"NET ",  "~/.local/bin/statusbar/network",     5,                   0},
	{"",  "~/.local/bin/statusbar/time",     60,                   0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
